import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('../views/publicados.vue'),
  },
  {
    path: '/rascunhos',
    component: () => import('../views/rascunhos.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
