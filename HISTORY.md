## História do usuário

### Tela [Publicados](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A823)
### Tela [Rascunhos](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A961)

**Como** usuário  
**quero** poder visualizar todos os meus formulários criados e publicados em lista  
**Para que** eu possa acessar e realizar ações em cada formulário.

## Critérios de aceitação

**Dado** que eu entrei na plataforma
**Quando** a página de `Gestão` carregar      
**Então** deve estar visíveis os botões de `Publicados`, `Rascunhos`, a barra e o botão de `Busca` e 
`Criar formulários`  
**E** o botão de `Publicados` deve estar habilitado e o a listagem de formulários publicado visivel   

**Dado** que acessei página de `Gestão`   
**Quando** a página de [Publicados](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A823) carregar     
**Então** deve estar listados os formulários publicados     
**E** os os cards dos formulários devem estar dispostos em duas colunas ordenados por 
publicação do formulário mais recente para o mais antigo, da esquerda para direita, de cima para baixo     
**E** em cada formulário é visível o `Título` (com no máximo duas linhas), 
`Descrição` (com no máximo duas linhas), data de criação `Criado`, publicação 
`Publicado`, tipo de `Acesso`,`Vigência`, `N°de respostas`, `Marcadores` conforme 
[tela](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A824)   
**E** deve conter os botões de `Compartilhar`, `Duplicar`, `Preencher`e `Detalhes`.   

**Dado** que acessei a página de `Gestão`   
**quando** tiver em formulários `Publicados`  
**E** não houver formulários listados    
**Então** deve estar visível a mensagem de "Nenhum fo rmulário publicado. 
Crie novos formulários ou publique formulário em rascunho" e o botão de 
`Criar formulário` conforme [tela](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A877)    

**Dado** que estou na página de `Gestão`    
**Quando** clicar em [Rascunho](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A961)  
**Então** deve estar listados os formulários em rascunho     
**E** os o cards dos formulários devem estar dispostos em duas colunas ordenados por 
criação do mais recente para o mais antigo, da esquerda para direita, de cima para baixo    
**E** em cada formulário é visível a `ID`do formulário, o `Título` (com no máximo duas linhas), 
`Descrição` (com no máximo duas linhas), data de criação `Criado`, `Marcadores` conforme 
[tela](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A979)   
**E** deve conter os botões de `Excluir`, `Compartilhar`, `Duplicar`, `Publicar`e `Detalhes`.  

**Dado** que acessei a página de `Gestão`   
**quando** tiver em formulários `Rascunhos`  
**E** não houver formulários listados   
**Então** deve estar visível a mensagem de "Nenhum formulário em rascunho. Crie novos formulários." 
e o botão de `Crie novos formulários` conforme 
[tela](https://www.figma.com/file/yr2dG5kSnLMF2XHO9NW1m6/Untitled?node-id=1%3A919)    


## Garantir que:
- [ ] Esteja listados os formulários publicados e em rascunhos;
- [ ] Seja possível alternar a visualização entre formulários publicados e rascunhos;
- [ ] Quando não houver formulários listados o usuário saiba que não tem formulários.
- [ ] A tela seja responsiva a diferentes tamanhos de telas
